#total words:
words_dict = {}
with open("data/exercise.txt", "rt") as file:
    for i, line in enumerate(file):
        clean_line = line.rstrip()
        clean_line = clean_line.lower()
        if clean_line == "":
            continue
        for char in (".", ",", "!", "?"):
            clean_line = clean_line.replace(char, "")

        words = clean_line.split(" ")

        for word in words:
            if words_dict.get(word) is not None:
                words_dict[word] += 1
            else:
                words_dict[word] = 1
print(words_dict)
with open("data/result.txt", "w+") as f:  # open file in write mode
    for word in words_dict:
        f.write(f"Word {word} occurred {words_dict[word]} time(s).\n")



